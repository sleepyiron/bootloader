Bootloader for the sleepyiron kernel.

More coming soon.


Compiling and running of this bootloader is done with NASM and Qemu

nasm -f bin bootsector.asm -o /tmp/boot.bin && qemu-system-x86_64 -fda /tmp/boot.bin
